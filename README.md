# RubyFrontier-Buch

![Logo](rubyfrontierlogo.png)

Nach einer langjährigen Pause habe ich beschlossen, das Projekt »RubyFrontier-Buch« wieder aufzunehmen und fortzuführen.

## Links

- [Das RubyFrontier-Buch online](http://rubyfrontier.kantel-chaos-team.de/)
- [RubyFrontier in meinem Wiki](http://www.cognitiones.de/doku.php/rubyfrontier)
- [RubyFrontier auf GitHub](https://github.com/mattneub/RubyFrontier)
- *Matt Neuburgs* [RubyFrontier-Dokumentation](http://www.apeth.com/RubyFrontierDocs/)